Your problems or questions may interest other people. Please, **do not contact me (Jflesch) by private message**. Please use the forum or the bug tracker.


## Forum

This is the place to go if:
* you have any questions regarding Paperwork, Pyocr, or Libinsane, etc
* you want to discuss a new functionality for Paperwork, Pyocr or Libinsane, etc
* you want to stay up-to-date regarding major changes on Paperwork (dependencies, etc)
* you want to get the release announcements first hand
* you don't know if something is a bug or a feature ;-)

https://forum.openpaper.work/

Please write your messages in English.

## Automatic bug reports

Since Paperwork 2.0, Paperwork includes a tool to submit automatic bug reports. This tool takes care of:
* Including all the required information (version of Paperwork, system info, ...).
* Censoring the attachments (logs, screenshots, ...) to protect your privacy.
* Letting you explain in your own words what went wrong.
* Sending all of those info to openpaper.work.
* Providing a link to your report. This link includes a protection key to make sure only the people having this link can access your report.

![paperwork_bug_report](uploads/dd60dd72f67befa6a663b68abea1a125/paperwork_bug_report.png)

This tool is very convenient to make sure all the required info are provided. However those bug reports are not so convenient to discuss with developers. We also get many more of them, and therefore they may be easily unintentionally dismissed. If you want to make sure your bug report is not unintentionally dismissed or if you want to discuss with Paperwork's developers, you're advised to open a ticket on Gitlab too (see below). You can add the link of your automated bug report in the Gitlab ticket. You should just make sure no private information has been accidentally included in the report ; for instance, please make sure the screenshots have been correctly censored.

If for some reason your automated bug report includes data you consider private, please contact [Jflesch](mailto:jflesch@openpaper.work). The report will be deleted as soon as possible (usually less than 48h).


## Bug trackers (Gitlab)

* [Paperwork's bug tracker](https://gitlab.gnome.org/World/OpenPaperwork/paperwork/issues)
* [Libinsane's bug tracker](https://gitlab.gnome.org/World/OpenPaperwork/libinsane/issues)
* [PyOCR's bug tracker](https://gitlab.gnome.org/World/OpenPaperwork/pyocr/issues)
* [Libpillowfight's bug tracker](https://gitlab.gnome.org/World/OpenPaperwork/libpillowfight/issues)

This is the place to go if Paperwork (or Libinsane or PyOCR) doesn't act as expected. This includes:
* Uncatched exceptions
* Crashes
* Unexpected or confusing behaviors from the GUI
* Feature requests (please make it as specific, clear and detailed as possible)

If you don't know which bug tracker is the most appropriate, please use Paperwork's bug tracker. I'll create others tickets in other bug trackers if required.

Please write bug reports or feature requests **in English**.

Here is some information usually useful in a good bug report:

* What is the problem ?
  * How to reproduce the problem ?
  * What was the expected result ?
  * What result did you get ?
* Diagnostic info:
  * What version of Paperwork are you using ? (master, testing or develop)
  * How did you install Paperwork ? (Flatpak, virtualenv, distribution package, ...)
  * What GNU/Linux distribution do you use ? / What version of Windows do you use ?

## IRC

Come chat a bit ! :-)

If you're not familiar with IRC, we suggest you use [Element](https://element.io/). Once you're registered on Matrix.io using Element, you can join the channel `#paperwork:libera.chat`.

If you're familiar with IRC, here are the settings you need to know:
Server: irc.libera.chat ; Port: 6697 (SSL) ; Channel: #983 

## Contributing

See [Contributing](Contributing).