### For users

You may also find information in Paperwork itself. There is an integrated manual (Application Menu -> Help -> User manual).

* [Readme](https://gitlab.gnome.org/World/OpenPaperwork/paperwork#readme)
* [Contact](Contact)


### For contributors

* Documentation
  * [Contributing to Paperwork](Contributing) (must-read for any contribution !)
  * [OpenPaperwork-core](https://doc.openpaper.work/openpaperwork_core/latest/index.html) (must-read for any code contribution !)
  * [Libinsane](https://doc.openpaper.work/libinsane/latest/)
  * [Work directory organization](Work-directory-organization)
  * [User manual](https://download.openpaper.work/data/paperwork/develop_latest/data.tar.gz) (paperwork-gtk/src/paperwork_gtk/model/help/out/usage.pdf)
* For translators: [WebLate instance](https://translations.openpaper.work/)
* [Builds status](https://gitlab.gnome.org/World/OpenPaperwork/paperwork/pipelines/)