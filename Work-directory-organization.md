You can also find this information in the user manual integrated in Paperwork. It may give you information more specific to the version of Paperwork you are using.

workdir|rootdir = ~/papers

# Importing existing documents

If you already have a lot of documents, you want to import them in Paperwork and you are familiar with scripting, you shouldn't try to replicate the file structure described here by yourself. Instead if possible, you're advised to use [Paperwork-shell](https://gitlab.gnome.org/World/OpenPaperwork/paperwork/-/blob/develop/paperwork-shell/README.markdown) (see [`paperwork-json import`](https://gitlab.gnome.org/World/OpenPaperwork/paperwork/-/blob/develop/paperwork-shell/README.markdown#import) / [`paperwork-cli import`](https://gitlab.gnome.org/World/OpenPaperwork/paperwork/-/blob/develop/paperwork-shell/README.markdown#import)).

# Global organisation

In the work directory, you have folders, one per document.

The folder names are (usually) the scan/import date of the documents:
`YYYYMMDD\_hhmm\_ss[\_<idx>]`. The suffix 'idx' is optional and is just
a number added in case of name collision. `idx` can be a string too.

Here is an example a work directory organization:

```sh
$ find ~/papers
/home/jflesch/papers
/home/jflesch/papers/20130505_1518_00
/home/jflesch/papers/20130505_1518_00/paper.1.jpg
/home/jflesch/papers/20130505_1518_00/paper.1.thumb.jpg
/home/jflesch/papers/20130505_1518_00/paper.1.words
/home/jflesch/papers/20130505_1518_00/paper.2.jpg
/home/jflesch/papers/20130505_1518_00/paper.2.thumb.jpg
/home/jflesch/papers/20130505_1518_00/paper.2.words
/home/jflesch/papers/20130505_1518_00/paper.3.jpg
/home/jflesch/papers/20130505_1518_00/paper.3.thumb.jpg
/home/jflesch/papers/20130505_1518_00/paper.3.words
/home/jflesch/papers/20130505_1518_00/labels
/home/jflesch/papers/20110726_0000_01
/home/jflesch/papers/20110726_0000_01/paper.1.jpg
/home/jflesch/papers/20110726_0000_01/paper.1.thumb.jpg
/home/jflesch/papers/20110726_0000_01/paper.1.words
/home/jflesch/papers/20110726_0000_01/paper.2.jpg
/home/jflesch/papers/20110726_0000_01/paper.2.edited.jpg  # Paperwork >= 2.0 only
/home/jflesch/papers/20110726_0000_01/paper.2.thumb.jpg
/home/jflesch/papers/20110726_0000_01/paper.2.words
/home/jflesch/papers/20110726_0000_01/extra.txt
/home/jflesch/papers/20130106_1309_44
/home/jflesch/papers/20130106_1309_44/doc.pdf
/home/jflesch/papers/20130106_1309_44/paper.1.words
/home/jflesch/papers/20130106_1309_44/paper.2.words
/home/jflesch/papers/20130106_1309_44/labels
/home/jflesch/papers/20130106_1309_44/extra.txt
/home/jflesch/papers/20130520_1309_44
/home/jflesch/papers/20130520_1309_44/doc.pdf
/home/jflesch/papers/20130520_1309_44/doc.docx
/home/jflesch/papers/20130520_1309_44/labels
```

# Document files

## Image documents

  * `paper.<X>.jpg`: A page in JPG format (X starts at 1). It's the original page (as scanned or imported).
  * `paper.<X>.edited.jpg` (optional): The page after post-processing and editing. (Paperwork >= 2.0 only)
  * `paper.<X>.words` (optional): A
    [hOCR](https://docs.google.com/document/d/1QQnIQtvdAC_8n92-LhwPcjtAUFwBlzE8EWnKAxlgVf0/preview)
    file, containing all the words found on the page using the OCR (optional, but required for indexing ; can be regenerated with the options "Redo OCR (...)").
  * `paper.<X>.thumb.jpg` (optional, generated and updated automatically): A thumbnail version of the page (faster to load).
    Starting with Paperwork 2.0, only paper.1.thumb.jpg is used.
  * `labels` (optional): a text file containing the labels applied on this document (text + label color)
  * `extra.txt` (optional): extra keywords added by the user

## PDF & converted documents (.docx, .odt, etc)

  * `doc.pdf`: the document
  * `labels` (optional): a text file containing the labels applied on this document
  * `extra.txt` (optional): extra keywords added by the user
  * `paper.<X>.words` (optional): A
    [hOCR](https://docs.google.com/document/d/1QQnIQtvdAC_8n92-LhwPcjtAUFwBlzE8EWnKAxlgVf0/preview)
    file, containing all the words found on the page using the OCR. Some PDF contains crap instead
    of the real text, so running the OCR on them can sometimes be useful.
  * `paper.<X>.edited.jpg` (optional): The page after editing. (Paperwork >= 2.0 only)
  * `page_map.csv` (optional): Created if the user move the page inside the PDF file. doc.pdf is not actually modified, only this mapping file. Pages are reordered on-the-fly when the document is displayed or exported. (Paperwork >= 2.0 only)
  * `passwd.txt` (optional): PDF password (if the PDF is password-protected ; Paperwork >= 2.1 only)
  *  doc.docx / doc.odt / ... (optional): Original file. Converted into PDF (doc.pdf) so Paperwork can parse and display it more quickly. (Paperwork >= 2.1 only)


# File details

## hOCR files

With Tesseract, the hOCR file can be obtained with following command:

```sh
tesseract paper.<X>.jpg paper.<X> -l <lang> hocr && mv paper.<X>.html paper.<X>.words
```

For example:

```sh
tesseract paper.1.jpg paper.1 -l fra hocr && mv paper.1.html paper.1.words
```

## Label files

Here is an example of content of a label file:

```
facture,#0000b1588c61
logement,#f6b6ffff0000
```

It's always [label],[color]. For a same label, the color should always be the same.
Commas cannot be used in label names.

### Page mapping

CSV headers are actually ignored by Paperwork.

Leep in mind that internally in Paperwork, all page numbers start at index 0. While the file names starts at index 1 (`paper.1.jpg`), the content of the CSV starts at 0.

Examples of `page_map.csv` below.

#### User has swapped page 3 and 4 in the PDF file

```csv
original_page_idx,target_page_idx
3,4
4,3
```

#### Page 3 of the PDF has been deleted by the user

```csv
original_page_idx,target_page_idx
3,-1
4,3
5,4
```

#### a JPEG page has been inserted in the middle of the PDF file as page 3

The JPEG page has the file name `paper.4.jpg`. If pushes all the pages of the PDF file further.

```csv
original_page_idx,target_page_idx
3,4
4,5
5,6
```